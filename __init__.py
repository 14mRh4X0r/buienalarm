import appglue
import easywifi
import easydraw
import ugfx

from . import buienradar

for _ in range(5):
    if easywifi.enable(): break

easydraw.msg("Loading rain forecast...")

ugfx.set_lut(ugfx.LUT_FULL)
ugfx.clear(ugfx.WHITE)
buienradar.draw_rain_graph(0, 0, 296, 128, 5.0, ugfx.BLACK)
ugfx.flush()

ugfx.input_init()
ugfx.input_attach(ugfx.BTN_START, lambda _: appglue.home())
